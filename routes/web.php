<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/data-table', 'AuthController@table');


Route::group(['middleware' => ['auth']], function () {
    //CRUD cast
    Route::get('/cast/create', 'castController@create'); //route menuju form create
    Route::post('/cast', 'castController@store'); // route untuk menyimpan data kedalam database
    Route::get('/cast/{cast_id}/edit', 'castController@edit'); //menuju form edit
    Route::put('/cast/{cast_id}', 'castController@update'); //menyimpan data edit berdasarkan id tertentu
    Route::delete('/cast/{cast_id}', 'castController@destroy'); //menghapus data cast
    Route::get('/cast', 'castController@index'); //route menampilkan data cast
    Route::get('/cast/{cast_id}', 'castController@show'); //menampilkan data berdasarkan id tertentu

    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
});


//CRUD Game
//create data game
Route::get('/game/create', 'gameController@create');
Route::post('/game','gameController@store');
//Menampilkan data game
Route::get('/game', 'gameController@index');
Route::get('/game/{game_id}', 'gameController@show');
//Edit data game
Route::get('/game/{game_id}/edit', 'gameController@edit');
Route::put('/game/{game_id}', 'gameController@update');
//Delete data game
Route::delete('/game/{game_id}', 'gameController@destroy');

Route::resource('film', 'FilmController');

Auth::routes();

