@extends('admin.master')

@section('judul')
<h3>Halaman Tambah Dat Film</h3>
@endsection

@section('content')
<form method="POST" action="/film" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" name="judul">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Ringkasan Film</label>
        <textarea name="ringkasan" class="form-control"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-roup">
        <label>Kategori Film</label>
        <select name="genre_id" class="form-control">
            <option value="">--Pilih Kategori Film--</option>
            @foreach ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
  </form>
@endsection