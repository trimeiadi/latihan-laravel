@extends('admin.master')

@section('judul')
<h3 class="card-title">Data Cast</h3>
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-warning mb-3">Tambah Data</a>
@endauth
<div class="row">
    @forelse ($film as $item)
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <h3 class="card-title"><b>{{$item->judul}} ({{$item->tahun}})</b></h3>
          <p class="card-text">{{$item->ringkasan}}</p>
          <form action="/cast/{{$item->id}}" method="post">
            @csrf
            @method('delete')
            <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
            @auth
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type='submit' class="btn btn-danger btn-sm" value="delete">    
            @endauth
          </form>
        </div>
      </div>
    </div>
    @empty
        
    @endforelse

@endsection