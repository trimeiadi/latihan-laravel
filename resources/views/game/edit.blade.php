<!doctype html>
<html lang="en">
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
<title>Edit Data</title>
</head>

<body>
<h2 class="ml-4">Edit Data Game</h2>
<form method="POST" action="/game/{{$game->id}}">
    @csrf
    @method('PUT')
    <div class="form-group ml-3">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$game->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group ml-3">
        <label>Gameplay</label>
        <input type="text" class="form-control" name="gameplay" value="{{$game->gameplay}}">
    </div>
    @error('gameplay')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group ml-3">
        <label>Developer</label>
        <input type="text" class="form-control" name="developer" value="{{$game->developer}}">
    </div>
    @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group col-md-2">
        <label>Tahun</label>
        <input type="text" class="form-control" name="year" value="{{$game->year}}">
    </div>
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary ml-3">Submit</button>
    <a href="/game" type="submit" class="btn btn-danger ml-3">Batal</a>
  </form>


<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>

