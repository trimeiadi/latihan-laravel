@extends('admin.master')

@section('judul')
    <h2>Buat Akun Baru</h2>
@endsection
    
@section('content')
    <h3>Sign Up Form</h3>
        <form action="welcome" method="post">
            @csrf
        <label for="">First Name :</label><br>
        <input type="text" name="firstName"><br><br>
        <label for="">Last Name :</label><br>
        <input type="text" name="lastName">
        <br><br>
        <label for="">Gender:</label>
        <br>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <br><br>
        <Label>Nationality :</Label>
        <br>
        <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        </select>
        <br><br>
        <label for="">Language spoken</label>
        <br>
        <input type="checkbox" name="Bahasa1" value="Indonesia">
        <label for="Bahasa1">Bahasa Indonesia</label><br>
        <input type="checkbox" name="Bahasa2" value="English">
        <label for="Bahasa2">English</label><br>
        <input type="checkbox" name="Bahasa3" value="Other">
        <label for="Bahasa3">Other</label>
        <br><br>
        <label for="">Bio:</label>
        <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
        </form>
@endsection
    
