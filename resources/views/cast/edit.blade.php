@extends('admin.master')

@section('judul')
<h3 class="card-title">Tambah Data Cast</h3>
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group col-md-2">
        <label>Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea type="text" class="form-control" name="bio">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection