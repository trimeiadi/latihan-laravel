@extends('admin.master')

@section('judul')
<h3 class="card-title">Detail Cast</h3>
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}} Tahun</p>
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-dark mb-3">Kembali</a>
@endsection