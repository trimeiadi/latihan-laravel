@extends('admin.master')

@section('judul')
<h3>Halaman Update Profile</h3>
@endsection

@section('content')
<form method="POST" action="/profile/{{$profile->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" name="umur" value="{{$profile->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" class="form-control" name="bio">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea type="text" class="form-control" name="alamat">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection